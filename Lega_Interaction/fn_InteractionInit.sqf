addMissionEventHandler ["PlayerConnected", {
    params ["_id", "_uid", "_name", "_jip", "_owner"];

    {
        if !(hasInterface) exitWith {};
        waitUntil {!isNull player};
        waitUntil{!isNull (findDisplay 46)};
        
        #define INTERACTIONUI (uiNamespace getVariable ["Lega_Interaction", displayNull])
        #define KEYDIK 220 // the dik code of the key you want to use.



        (findDisplay 46) displayAddEventHandler ["keydown", {
            params ["_control", "_key", "_shift", "_ctrl", "_alt"];
            if (_key isEqualTo KEYDIK) then {
                if (isNull INTERACTIONUI) then {[] call Lega_fnc_openInteraction};
            };
        }];

        Lega_Interaction_Debug = true;
        Lega_InteractionOpen = false;
        Lega_InteractionOptions = [
            [
                "Option Text",
                "Category",
                {true},
                {systemChat "test"},
                "colorRed"
            ],
            [
                "Option Text",
                "Category",
                {true},
                {systemChat "test"},
                "colorBlack"
            ]
        ];

        Lega_fnc_openInteraction = {

            if (Lega_InteractionOpen) exitWith {};
            Lega_InteractionOpen = true;
            if (isNull INTERACTIONUI) then {
                uiNamespace setVariable ["Lega_Interaction", (
                    (findDisplay 46 createDisplay "RscDisplayEmpty")
                )];
            };

            INTERACTIONUI displayAddEventHandler ["keyup", {
                params ["_control", "_key", "_shift", "_ctrl", "_alt"];
                if (_key isEqualTo KEYDIK) exitWith {
                    if !(isNull INTERACTIONUI) then {INTERACTIONUI closeDisplay 1;Lega_InteractionOpen = false};
                };
            }];

            Lega_activeButtons = [];
            Lega_activeButtonPositions = [];

            private _controlsGroup = INTERACTIONUI ctrlCreate ["RscControlsGroup", 321321];
            _controlsGroup ctrlSetPosition [
                0.4275 * safezoneW + safezoneX,
                0.3 * safezoneH + safezoneY,
                0.12375 * safezoneW,
                (0.363 * (safezoneH / 15) * 30)
            ];
            _controlsGroup ctrlCommit 0;
            private _startBtnIdc = 9995;

            {
                _x params [
                    ["_displayText", "", [""]],
                    ["_displayText", "", [""]],
                    ["_conditions", {}, [{}]],
                    ["_onCall", {}, [{}]],
                    ["_color", "colorBlack", [""]]
                ];

                private _newControl = INTERACTIONUI ctrlCreate ["RscButtonMenu", _startBtnIdc, _controlsGroup];
                _newControl ctrlSetFade 1;
                _newControl ctrlCommit 0;

                _startBtnIdc = _startBtnIdc + 1;
                _newControl ctrlSetText _displayText;
                _newControl ctrlSetBackgroundColor ((configProperties [configFile >> "CfgMarkerColors" >> _color] # 1) call BIS_fnc_colorConfigToRGBA);
                _newControl ctrlSetPosition [0 ,0 , 0.12375 * safezoneW, 0];
                _newControl ctrlCommit 0;

                private _positionToSet = [0 ,0 , 0.12375 * safezoneW, 0.363 * (safezoneH / 15)];

                if !((count Lega_activeButtons) isEqualTo 0) then {
                    private _lastPos = Lega_activeButtonPositions # (count Lega_activeButtonPositions - 1);
                    _positionToSet = [0, ((_lastPos # 1) + ((0.363 * (safezoneH / 15)) * 1.2)), 0.12375 * safezoneW, 0.363 * (safezoneH / 15)];
                };

                _newControl ctrlSetPosition _positionToSet;
                _newControl ctrlSetFade 0;
                _newControl ctrlCommit 0.2;
                _newControl setVariable ["interactionCode", _oncall];
                _newControl ctrlAddEventHandler ["ButtonClick", {
                    params ["_ctrl"];
                    private _code = _ctrl getVariable ["interactionCode", {}];
                    call _code;
                    INTERACTIONUI closeDisplay 1;
                    Lega_InteractionOpen = false;
                }];
                Lega_activeButtons pushBack _newControl;
                Lega_activeButtonPositions pushBack _positionToSet;
            } forEach Lega_InteractionOptions;
        };

    } remoteExec ["BIS_fnc_call", _owner];
}];



