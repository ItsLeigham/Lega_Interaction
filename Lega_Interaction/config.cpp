class CfgPatches 
{
    class Lega_Interaction 
	{
        units[] = {"C_man_1"};
        weapons[] = {};
        requiredAddons[] = {"A3_Data_F"};
        fileName = "Lega_Interaction.pbo";
        author = "Leigham";
    };
};

class cfgFunctions
{
	  class Lega_Interaction_Functions
	  {
			tag = "Lega";
			class Init
			{
				file = "\Lega_Interaction";
				class InteractionInit {
					preInit=1;
				};
			};    
	  };
};